﻿using UserStore.DAL.Entities;
using Microsoft.AspNet.Identity;

namespace UserStore.DAL.Identity
{
    // edit users: update users db
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
                : base(store)
        {
        }
    }
}