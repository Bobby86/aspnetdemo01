﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace UserStore.DAL.Entities
{
    // 1to1 to ApplicationUser
    public class ClientProfile
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

        public string Name { get; set; }        // login name
        public DateTime LastLoginDateTime { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
