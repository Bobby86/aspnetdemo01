﻿using UserStore.DAL.EF;
using UserStore.DAL.Entities;
using UserStore.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;
using UserStore.DAL.Identity;

namespace UserStore.DAL.Repositories
{
    //  contain references to managers and repository of users
    public class IdentityUnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;

        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;
        private IClientManager clientManager;

        public IdentityUnitOfWork(string connectionString)
        {
            db = new ApplicationContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            clientManager = new ClientManager(db);
            
            //CreateAdmin (roleManager, userManager);
        }

        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public IClientManager ClientManager
        {
            get { return clientManager; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }
        
        //private void CreateAdmin(ApplicationRoleManager _roleManager, ApplicationUserManager _userManager)
        //{
        //    if (!roleManager.RoleExists("admin"))
        //    {
        //        var role = new ApplicationRole();
        //        role.Name = "admin";
        //        var roleResult = _roleManager.Create(role);

        //        if (!roleResult.Succeeded)
        //        {
        //            //System.Diagnostics.Debug.WriteLine("UserStore.DAL.Repositories.IdentityUnitOfWork.CreateAdmin: admin role was not successfully created");
        //            return;
        //        }
        //    }
        //    //else
        //    //{
        //    //    System.Diagnostics.Debug.WriteLine("admin role are exist");
        //    //}

        //    if (_userManager.FindByName("SuperAdmin") == null)
        //    {
        //        var user = new ApplicationUser();
        //        user.UserName = "SuperAdmin";
        //        var userResult = _userManager.Create(user, "Admin_667");

        //        if (userResult.Succeeded)
        //        {
        //            _userManager.AddToRole(user.Id, "admin");
        //        }
        //        //else
        //        //{
        //        //    System.Diagnostics.Debug.WriteLine("UserStore.DAL.Repositories.IdentityUnitOfWork.CreateAdmin: SuperAdmin was not successfully created");
        //        //}
        //    }
        //}

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    clientManager.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}