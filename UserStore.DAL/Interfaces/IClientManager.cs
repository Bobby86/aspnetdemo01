﻿using UserStore.DAL.Entities;
using System;

namespace UserStore.DAL.Interfaces
{
    // controll users profiles
    public interface IClientManager : IDisposable
    {
        void Create(ClientProfile item);
        void Remove(ClientProfile item);
    }
}