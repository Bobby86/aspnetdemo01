﻿using UserStore.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace UserStore.DAL.Interfaces
{
    // contain references to user and role managers and users repository
    public interface IUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        IClientManager ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
    }
}