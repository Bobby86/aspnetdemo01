﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using DemoWebApp01.Models;
using UserStore.BLL.DTO;
using System.Security.Claims;
using UserStore.BLL.Interfaces;
using UserStore.BLL.Infrastructure;

namespace DemoWebApp01.Controllers
{
    public class AccountController : Controller
    {
        #region References
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        #endregion

        #region Login part
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                AuthenticationManager.SignOut();
                ViewBag.Signed = "Sign In";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Signed = "Sign Out";                     
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            await SetInitialDataAsync();

            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO { UserName = model.Name, Password = model.Password, LastLoginDateTime = DateTime.Now };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);

                if (claim == null)
                {
                    ViewBag.Signed = "Sign In";
                    ModelState.AddModelError("", "Login or password are not valid.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    ViewBag.Signed = "Sign Out";
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            ViewBag.Signed = "Sign In";
            return RedirectToAction("Index", "Home");
        }
        #endregion

        #region Registration part
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                // u r signed in
                ViewBag.Signed = "";
            }
            else
            {
                ViewBag.Signed = "Sign In";
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            await SetInitialDataAsync();

            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Password = model.Password,
                    UserName = model.Name,
                    Role = "user",
                    LastLoginDateTime = DateTime.Now
                };
                OperationDetails operationDetails = await UserService.Create(userDto);

                if (operationDetails.Succedeed)
                {
                    return RedirectToAction("Login", "Account");
                    //return View("RegistrationSuccessful");
                }
                else
                {
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
            }

            return View(model);
        }

        private async Task SetInitialDataAsync()
        {
            await UserService.SetInitialData(new UserDTO
            {
                UserName = "SuperAdmin",
                Password = "Admin_667",
                Role = "admin",
                LastLoginDateTime = DateTime.Now
            }, new List<string> { "user", "admin" });
        }
        #endregion
    }
}