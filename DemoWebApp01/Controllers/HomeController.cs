﻿using System.Web.Mvc;
using DemoWebApp01.Util;

namespace DemoWebApp01.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.Signed = "Sign Out";
            }
            else
            {
                ViewBag.Signed = "Sign In";
            }

            return View();
        }
    }
}