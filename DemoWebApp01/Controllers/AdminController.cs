﻿using System.Threading.Tasks;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using UserStore.BLL.Interfaces;
using UserStore.BLL.DTO;
using Microsoft.AspNet.Identity.Owin;

namespace DemoWebApp01.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private IUserService userService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public ActionResult Index()
        {
            return View("~/Views/Admin/AdminMain.cshtml");
        }

        public async Task<ActionResult> EditUsers ()
        {
            IEnumerable<UserNameRoleDTO> userNRDTOList = await userService.GetUsersByLoginRoles(); //list of UserNAmeRoleDTO objects
            ViewBag.usersNameRolesDTOList = userNRDTOList;
            return View("~/Views/Admin/EditUsers.cshtml");
        }
    }
}