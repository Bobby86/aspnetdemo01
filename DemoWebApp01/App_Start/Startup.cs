﻿using Microsoft.Owin;
using Owin;
//using DemoWebApp01.Models;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using UserStore.BLL.Services;
using UserStore.BLL.Interfaces;

[assembly: OwinStartup (typeof (DemoWebApp01.App_Start.Startup))]

namespace DemoWebApp01.App_Start
{
    public class Startup
    {
        IServiceCreator serviceCreator = new ServiceFactory();  // creates UserService <- IdentityUnitOfWork
        
        public void Configuration(IAppBuilder app)
        {
            // configure a context and a manager
            app.CreatePerOwinContext<IUserService>(CreateUserService);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
            });
        }

        private IUserService CreateUserService()
        {
            return serviceCreator.CreateUserService("IdentityDb");
        }
    }
}