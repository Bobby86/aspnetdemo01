﻿using System.ComponentModel.DataAnnotations;

namespace DemoWebApp01.Models
{
    public class RegisterModel
    {
        //TODO error messages and filters
        [Required]
        public string Name { get; set; }

        //[Required]
        //public int Year { get; set; }

        [Required]
        [DataType (DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare ("Password", ErrorMessage = "Password does not match")]
        [DataType (DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}