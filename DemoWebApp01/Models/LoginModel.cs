﻿using System.ComponentModel.DataAnnotations;

namespace DemoWebApp01.Models
{
    public class LoginModel
    {
        //TODO error messages and filters

        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}