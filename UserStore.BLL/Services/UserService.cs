﻿using System;
using System.Data.Entity;
using UserStore.BLL.DTO;
using UserStore.BLL.Infrastructure;
using UserStore.DAL.Entities;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using UserStore.BLL.Interfaces;
using UserStore.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace UserStore.BLL.Services
{
    // communication with db with unitOfWork object: registration/authentication/init db
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }  // communication with a db

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            // search for a user by login
            ApplicationUser user = await Database.UserManager.FindByNameAsync(userDto.UserName);

            if (user == null)
            {
                user = new ApplicationUser { UserName = userDto.UserName };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);

                if (result.Errors.Count() > 0)
                {
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                }
                // add role
                await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
                // create user profile
                ClientProfile clientProfile = new ClientProfile { Id = user.Id,  Name = userDto.UserName, LastLoginDateTime = DateTime.Now };
                Database.ClientManager.Create(clientProfile);
                await Database.SaveAsync();

                return new OperationDetails(true, "Registration Complete", "");
            }
            else
            {
                return new OperationDetails(false, "User with the same login already exist", "Name");
            }
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;
            // search for a user
            ApplicationUser user = await Database.UserManager.FindAsync(userDto.UserName, userDto.Password);
            // authenticate a user and return ClaimsIdentity object
            if (user != null)
            {
                claim = await Database.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            }
            return claim;
        }

        // remove user [admin only]
        public async Task<bool> RemoveUser (string userName)
        {
            var user = await Database.UserManager.FindByNameAsync(userName);

            if (user != null)
            {
                var result = await Database.UserManager.DeleteAsync(user);

                if (result.Succeeded)
                {
                    return true;
                }
            }
            return false;
        }

        // get collection of all users with roles for admin useredit page
        public async Task<IEnumerable<UserNameRoleDTO>> GetUsersByLoginRoles()
        {
            var result = await Database.UserManager
                .Users.Include("Roles").Select(user =>
                    new UserNameRoleDTO
                    {
                        UserName = user.UserName,
                        UserRoles = user.Claims.Where(c => c.ClaimType == ClaimTypes.Role)
                            .Select(c => c.ClaimValue)
                            .ToList(),
                    }).ToListAsync();

            System.Diagnostics.Debug.WriteLine("Test of BLL.UserService.GetUsersByLoginRoles: " + result.Count.ToString());

            return result;
        }

        // start initialization of db
        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);

                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    await Database.RoleManager.CreateAsync(role);
                }
            }
            await Create(adminDto);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}