﻿namespace UserStore.BLL.Infrastructure
{
    // contains information about operation
    public class OperationDetails
    {
        public OperationDetails(bool succedeed, string message, string prop)
        {
            Succedeed = succedeed;
            Message = message;
            Property = prop;
        }
        public bool Succedeed { get; private set; }
        public string Message { get; private set; }     // error message
        public string Property { get; private set; }    // error property
    }
}