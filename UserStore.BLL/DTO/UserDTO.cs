﻿using System;

namespace UserStore.BLL.DTO
{
    // data transfer object. transfer a data to/from a view level
    public class UserDTO
    {
        public string Id { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }    // login name
        public string Role { get; set; }
        public DateTime LastLoginDateTime { get; set; }
    }
}