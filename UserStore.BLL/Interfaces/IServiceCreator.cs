﻿namespace UserStore.BLL.Interfaces
{
    // the factory for UserService // dependencies injection (instead of Ninject package)
    public interface IServiceCreator
    {
        IUserService CreateUserService(string connection);
    }
}