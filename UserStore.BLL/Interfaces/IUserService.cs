﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using UserStore.BLL.DTO;
using UserStore.BLL.Infrastructure;

namespace UserStore.BLL.Interfaces
{
    // comunication between a view level and a data access level(DAL)
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDto);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task<bool> RemoveUser(string userName);
        Task<IEnumerable<UserNameRoleDTO>> GetUsersByLoginRoles();
        Task SetInitialData(UserDTO adminDto, List<string> roles);
    }
}