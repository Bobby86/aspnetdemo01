﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DemoWebApp01.Controllers;
using System.Web.Mvc;

namespace DemoWebApp01.Tests.Controllers
{
    [TestClass]
    class HomeControllerTest
    {
        private HomeController controller;
        private ViewResult result;

        [TestInitialize]
        public void Initializer ()
        {
            controller = new HomeController();
            result = controller.Index() as ViewResult;
        }

        [TestMethod]
        public void IndexViewResultNotNull ()
        {
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IndexViewEqualIndexCshtml ()
        {
            Assert.AreEqual("Index", result.ViewName);
        }
    }
}
